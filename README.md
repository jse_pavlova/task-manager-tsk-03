# TASK MANAGER 

Console application for task list.

# DEVELOPER INFO 

NAME: Victoria Pavlova

E-MAIL: vpavlova@tsconsulting.com

# SOFTWARE 

* JDK 1.8 

* Windows 10

# HARDWARE

* RAM 16Gb

* CPU i5

* HDD 476Gb

# RUN PROGRAM

'''
java -jar ./task-manager.jar
'''



#SCREENSHOTS 

https://yadi.sk/i/uHUZRXr8ESrhOQ

https://yadi.sk/i/PARzJgyly7092Q

https://yadi.sk/i/FFptxyBczKK4eA